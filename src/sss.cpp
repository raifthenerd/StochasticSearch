#include "ss.hpp"

using namespace std;

enum actions {ADD, DELETE, REPLACE};


// [[Rcpp::export]]
Rcpp::List sss(const arma::vec& y, const arma::mat& X,
               const arma::uvec& baseModel, unsigned int iter,
               unsigned int length)
{
    if (y.n_rows == 0 || y.n_cols == 0 || X.n_rows == 0 || X.n_cols == 0)
        throw invalid_argument("invalid design matrix");
    unsigned int n {X.n_rows}, p {X.n_cols - 1}, idx {};
    set<unsigned int> model {};
    double BF {modelLogBF(y, X, model)};
    map<set<unsigned int>,double> cache {{model, BF}};
    vector<pair<set<unsigned int>,double>> result {{model, BF}};
    double sumBFs {exp(BF)};
    for (unsigned int i = 1; i <= p; ++i) {
        model.clear();
        model.insert(i);
        BF = modelLogBF(y, X, model);
        cache[model] = BF;
        result.push_back(make_pair(model, BF));
        sumBFs += exp(BF);
    }
    model = set<unsigned int>(baseModel.begin(), baseModel.end());
    BF = modelLogBF(y, X, model);
    cache[model] = BF;
    result.push_back(make_pair(model, BF));
    sumBFs += exp(BF);

    for (unsigned int i = 0; i < iter; ++i) {
        vector<set<unsigned int>> modelAdd {}, modelRep {}, modelDel {};
        vector<double> cdfAdd {}, cdfRep {}, cdfDel {};
        double sumAdd {}, sumRep {}, sumDel {};
        for (unsigned int j = 1; j <= p; ++j) {
            set<unsigned int> tmp (model.begin(), model.end());
            bool isAdd {tmp.find(j) == tmp.end()};
            if (isAdd) {
                tmp.insert(j);
                modelAdd.push_back(tmp);
            } else {
                tmp.erase(j);
                modelDel.push_back(tmp);
            }
            auto it = cache.find(tmp);
            if (it == cache.end()) {
                BF = modelLogBF(y, X, tmp);
                cache[tmp] = BF;
                result.push_back(make_pair(tmp, BF));
                BF = exp(BF);
                sumBFs += BF;
            } else
                BF = exp(it->second);
            if (isAdd) {
                sumAdd += BF;
                cdfAdd.push_back(sumAdd);
            } else {
                sumDel += BF;
                cdfDel.push_back(sumDel);
            }
        }
        for (auto &j : model) {
            for (auto &K : modelDel) {
                set<unsigned int> tmp(K.begin(), K.end());
                tmp.insert(j);
                modelRep.push_back(tmp);
                auto it = cache.find(tmp);
                if (it == cache.end()) {
                    BF = modelLogBF(y, X, tmp);
                    cache[tmp] = BF;
                    result.push_back(make_pair(tmp, BF));
                    BF = exp(BF);
                    sumBFs += BF;
                } else
                    BF = exp(it->second);
                sumRep += BF;
                cdfRep.push_back(sumRep);
            }
        }
        unsigned int add {}, del {}, rep {};
        try {
            add = sampleCdf(cdfAdd);
            del = sampleCdf(cdfDel);
            rep = sampleCdf(cdfRep);
            switch(static_cast<actions>(samplePdf({cdfAdd[add],
                                                   cdfDel[del],
                                                   cdfRep[rep]}))) {
            case ADD:
                model = modelAdd[add];
                break;
            case DELETE:
                model = modelDel[del];
                break;
            case REPLACE:
                model = modelRep[rep];
                break;
            }
        } catch (const exception&) {
            if (cdfAdd.size() == 0)
                model = modelDel[del];
            else if (cdfDel.size() == 0)
                model = modelAdd[add];
        }
        sort(result.begin(), result.end(),
                  [](pair<set<unsigned int>,double> const & lhs,
                     pair<set<unsigned int>,double> const & rhs) {
                         return (lhs.second > rhs.second);
                     });
        if (result.size() > length)
            result.resize(length);
    }

    vector<double> probs(length);
    Rcpp::List models(length);
    for (unsigned int i = 0; i < length; ++i) {
        models[i]= result[i].first;
        probs[i] = exp(result[i].second)/sumBFs;
    }
    return Rcpp::List::create(Rcpp::_["model.vars"] = models,
                              Rcpp::_["model.probs"] = probs);
}
