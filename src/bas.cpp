#include "ss.hpp"

using namespace std;

static const double EPS {0.025};


// [[Rcpp::export]]
Rcpp::List bas(const arma::vec& y, const arma::mat& X, arma::vec ps,
               unsigned int iter, unsigned int interval)
{
    if (y.n_rows == 0 || y.n_cols == 0 || X.n_rows == 0 || X.n_cols == 0)
        throw invalid_argument("invalid design matrix");
    unsigned int n {X.n_rows}, p {X.n_cols - 1}, idx {};
    for (auto& prob : ps) prob = (1.0-2.0*EPS)*prob+EPS;
    map<vector<bool>,double> bintree {}, bintreetmp {};
    double sumQs {};
    vector<double> qs(p);
    list<vector<bool>> cache {};
    for (unsigned int i = 1; i <= iter; ++i) {
        vector<bool> model {};
        set<unsigned int> indices {};
        for (unsigned int j = 0; j < p; ++j) {
            auto it = bintreetmp.find(model);
            if (it == bintreetmp.end()) {
                bintreetmp[model] = ps[j];
                bool b = coinToss(ps[j]);
                model.push_back(b);
                if (b) indices.insert(j+1);
            } else {
                bool b = coinToss(it->second);
                model.push_back(b);
                if (b) indices.insert(j+1);
            }
        }
        cache.push_front(model);
        double BF {exp(modelLogBF(y, X, indices))};
        for (auto k : indices) qs[k-1] += BF;
        sumQs += BF;
        double prob {1.0};
        while (!model.empty()) {
            bool b {model.back()};
            model.pop_back();
            double& oldp = bintreetmp[model];
            prob *= b ? oldp : (1.0-oldp);
            if (b) oldp -= prob;
            oldp /= 1.0-prob;
        }
        if (i%interval == 0) {
            arma::vec tmp(p);
            for (unsigned int j = 0; j < p; ++j)
                tmp[j] = (1.0-2.0*EPS)*(qs[j]/sumQs)+EPS;
            if (arma::norm(ps-tmp) > sqrt(p)*sqrt(sqrt(EPS))) {
                ps = tmp;
                bintreetmp.clear();
                for (auto& m : cache) {
                    double prob {1.0};
                    while (!m.empty()) {
                        bool b {m.back()};
                        m.pop_back();
                        auto it = bintreetmp.find(m);
                        if (it == bintreetmp.end())
                            bintreetmp[m] = ps[m.size()];
                        double& oldp = bintreetmp[m];
                        prob *= b ? oldp : (1.0-oldp);
                        if (b) oldp -= prob;
                        oldp /= 1.0-prob;
                    }
                }
            }
        }
    }
    for (unsigned int j = 0; j < p; ++j)
        qs[j] /= sumQs;
    return Rcpp::List::create(Rcpp::_["inc.probs"] = qs);
}
