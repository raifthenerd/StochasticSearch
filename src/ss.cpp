#include "ss.hpp"

using namespace std;


double ssr (const arma::vec& y, const arma::mat& X)
{
    arma::mat Xt = X.t();
    arma::mat tmp = X*arma::inv(Xt*X)*Xt;
    double n = static_cast<double>(y.n_rows);
    tmp *= n/(n+1.0);
    return arma::as_scalar(y.t()*(arma::eye(y.n_rows, y.n_rows)-tmp)*y);
}

double modelLogBF (const arma::vec& y, const arma::mat& X,
                   const set<unsigned int>& model)
{
    double res {};
    if (model.size() == 0) return res;
    int n {static_cast<int>(y.n_rows)}, p {static_cast<int>(model.size())};
    // res += log(pi/(1.0-pi))*static_cast<double>(p)
    res -= .5*static_cast<double>(p)*log(1+n);
    arma::mat modelX = modelMat(X, model);
    double varEmpty {arma::var(y-arma::mean(y))};
    double varModel {arma::var(residual(y, modelX))};
    res += .5*(log(varModel)-log(varEmpty));
    varEmpty += arma::sum(y%y)-arma::sum(y)*arma::sum(y)*1.0/(n+1);
    varModel += ssr(y, modelX);
    res += .5*static_cast<double>(n+1)*(log(varEmpty)-log(varModel));
    return res;
}

unsigned int sampleCdf (const vector<double>& cdf)
{
    if (cdf.size() == 0)
        throw length_error("can't sample from empty cdf");
    double f {sampleUnif(*(cdf.rbegin()))};
    unsigned int tmp {}, lower {}, upper {cdf.size()-1};
    while (lower != upper) {
        tmp = (lower+upper)/2;
        if (f <= cdf[tmp])
            upper = tmp;
        else
            lower = tmp+1;
    }
    return lower;
}

unsigned int samplePdf (const vector<double>& pdf)
{
    if (pdf.size() == 0)
        throw length_error("can't sample from empty pdf");
    vector<double> cdf (pdf.begin(), pdf.end());
    for (auto it = cdf.begin(); it != cdf.end(); ++it) {
        if (it != cdf.begin()) {
            *it += *(it-1);
        }
    }
    return sampleCdf(cdf);
}
