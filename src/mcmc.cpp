#include "ss.hpp"

using namespace std;

// [[Rcpp::export]]
Rcpp::List mcmc(const arma::vec& y, const arma::mat& X,
                const arma::uvec& baseModel, unsigned int iter,
                unsigned int burn, unsigned int length)
{
    if (y.n_rows == 0 || y.n_cols == 0 || X.n_rows == 0 || X.n_cols == 0)
        throw invalid_argument("invalid design matrix");
    const unsigned int n {X.n_rows}, p {X.n_cols - 1};
    vector<unsigned int> ps(p, 0);

    map<vector<bool>,double> cache {};
    map<vector<bool>,unsigned int> counts {};

    set<unsigned int> tmp (baseModel.begin(), baseModel.end());
    double logit {}, foo {}, bar {modelLogBF(y, X, tmp)};

    vector<unsigned int> idx(p);
    for (unsigned int i = 0; i < p; ++i) idx[i] = i;

    vector<bool> model(p, false);
    for (auto i : baseModel) model[i-1] = true;

    for (unsigned int i = 0; i < iter + burn; ++i) {
        random_shuffle(idx.begin(), idx.end());
        for (auto j : idx) {
            vector<bool> mp (model);
            mp[j] = !mp[j];
            auto it = cache.find(mp);
            if (it == cache.end()) {
                tmp.clear();
                for (unsigned int k = 0; k < p; ++k)
                    if (mp[k]) tmp.insert(k+1);
                foo = modelLogBF(y, X, tmp);
                cache[mp] = foo;
            } else {
                foo = it->second;
            }
            logit = mp[j] ? foo-bar : bar-foo;
            model[j] = coinToss(1.0/(1.0+exp(-logit)));
            if (model[j] == mp[j]) foo = bar;
            if (model[j] && i >= burn) ++ps[j];
        }
        if (i >= burn) {
            auto it = counts.find(model);
            if (it == counts.end())
                counts[model] = 1;
            else
                ++(it->second);
        }
    }
    vector<pair<vector<bool>,unsigned int>> buf(counts.begin(), counts.end());
    sort(buf.begin(), buf.end(),
              [](pair<vector<bool>,unsigned int> const & lhs,
                 pair<vector<bool>,unsigned int> const & rhs) {
                     return (lhs.second > rhs.second);
                 });
    length = buf.size() > length ? length : buf.size();
    vector<double> probs(length);
    Rcpp::List models(length);
    for (unsigned int i = 0; i < length; ++i) {
        tmp.clear();
        for (unsigned int j = 0; j < p; ++j)
            if (buf[i].first[j]) tmp.insert(j+1);
        models[i] = tmp;
        probs[i] = static_cast<double>(buf[i].second)/static_cast<double>(iter);
    }
    vector<double> qs(p);
    for (unsigned int i = 0; i < p; ++i)
        qs[i] = static_cast<double>(ps[i])/static_cast<double>(iter);
    return Rcpp::List::create(
        Rcpp::_["model.vars"] = models,
        Rcpp::_["model.probs"] = probs,
        Rcpp::_["inc.probs"] = qs);
}
