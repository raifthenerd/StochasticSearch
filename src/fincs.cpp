#include "ss.hpp"

using namespace std;

static const double C {0.01};

static const vector<double> moveProbs {0.1, 0.85, 0.05};
enum moves {LOCAL, REPLACE, GLOBAL};


// [[Rcpp::export]]
Rcpp::List fincs(const arma::vec& y, const arma::mat& X,
                 const arma::uvec& baseModel, unsigned int iter,
                 unsigned int length, bool onlyReplace, bool gPrior)
{
    if (y.n_rows == 0 || y.n_cols == 0 || X.n_rows == 0 || X.n_cols == 0)
        throw invalid_argument("invalid design matrix");
    if (!onlyReplace && !gPrior)
        throw invalid_argument("can't use path-based prior with global moves");
    unsigned int n {X.n_rows}, p {X.n_cols - 1}, idx {};
    set<unsigned int> model (baseModel.begin(), baseModel.end());
    double logBF {};
    if (gPrior)
        logBF = modelLogBF(y, X, model);
    double sumBFs {exp(logBF)};
    vector<double> qs(p);
    for (auto i : model)
        if (i == 0 || i > p) throw invalid_argument("wrong model index");
        else qs[i-1] += exp(logBF);
    map<set<unsigned int>,double> result {{model, logBF}};
    vector<set<unsigned int>> modelPool {model};
    vector<double> cdf {exp(logBF)};

    for (unsigned int i = 1; i < iter; ++i) {
        bool sampled {false};
        set<unsigned int> smaller {}, larger {};
        bool isAdd {};
        while (!sampled) {
            vector<set<unsigned int>> modelAdd {}, modelDel {};
            vector<double> qsAdd {}, qsDel {};
            moves m {};
            if (onlyReplace)
                m = REPLACE;
            else
                m = static_cast<moves>(samplePdf(moveProbs));
            switch(m) {
            case REPLACE:
                idx = sampleCdf(cdf);
            case LOCAL:
                for (unsigned int j = 1; j <= p; ++j) {
                    set<unsigned int> tmp = modelPool[idx];
                    if (tmp.find(j) == tmp.end()) {
                        tmp.insert(j);
                        if (result.find(tmp) == result.end()) {
                            modelAdd.push_back(tmp);
                            double p {qs[j-1]/sumBFs};
                            qsAdd.push_back((p+C)/(1.0-p+C));
                        }
                    } else {
                        tmp.erase(j);
                        if (result.find(tmp) == result.end()) {
                            modelDel.push_back(tmp);
                            double p {qs[j-1]/sumBFs};
                            qsDel.push_back((1.0-p+C)/(p+C));
                        }
                    }
                }
                if (qsAdd.size() == 0 && qsDel.size() == 0) {
                    double p {};
                    if (idx > 0)
                        p = cdf[idx-1];
                    unsigned int size {cdf.size()};
                    for (unsigned int j = idx+1; j < size; ++j) {
                        p += result.find(modelPool[j])->second;
                        cdf[j] = p;
                    }
                    cdf.erase(cdf.begin()+idx);
                    modelPool.erase(modelPool.begin()+idx);
                } else {
                    if (qsAdd.size() == 0)
                        isAdd = false;
                    else if (qsDel.size() == 0)
                        isAdd = true;
                    else
                        isAdd = coinToss(0.5);
                    if (isAdd) {
                        smaller = modelPool[idx];
                        larger = modelAdd[samplePdf(qsAdd)];
                        model = larger;
                    } else {
                        larger = modelPool[idx];
                        smaller = modelDel[samplePdf(qsDel)];
                        model = smaller;
                    }
                    sampled = true;
                }
                break;
            case GLOBAL:
                model.clear();
                for (unsigned int j = 1; j <= p; ++j)
                    if (qs[j-1] > sampleUnif(sumBFs))
                        model.insert(j);
                if (result.find(model) == result.end())
                    sampled = true;
            }
        }
        if (gPrior)
            logBF = modelLogBF(y, X, model);
        else {
            double ratio = log(arma::norm(residual(y, modelMat(X,smaller))));
            ratio -= log(arma::norm(residual(y, modelMat(X,larger))));
            int d = 1+static_cast<int>(smaller.size());
            double lambda = (d+2.0)*(exp(2.0*ratio)-1.0);
            double tmp = ratio*(n-d);
            tmp += 0.5*log((d+2.0)/n);
            tmp += log(1.0-exp(-lambda/2.0));
            tmp -= log(lambda);
            if (isAdd)
                logBF = result[smaller]+tmp;
            else
                logBF = result[larger]-tmp;
        }

        modelPool.push_back(model);
        idx = modelPool.size()-1;
        result[model] = logBF;
        logBF = exp(logBF);
        cdf.push_back(*(cdf.rbegin())+logBF);
        sumBFs += logBF;
        for (auto j : model)
            if (j == 0 || j > p) throw invalid_argument("wrong model index");
            else qs[j-1] += logBF;
    }

    vector<pair<set<unsigned int>,double>> buf(result.begin(), result.end());
    sort(buf.begin(), buf.end(),
              [](pair<set<unsigned int>,double> const & lhs,
                 pair<set<unsigned int>,double> const & rhs) {
                     return (lhs.second > rhs.second);
                 });

    vector<double> probs(length);
    Rcpp::List models(length);
    for (unsigned int i = 0; i < length; ++i) {
        models[i] = buf[i].first;
        probs[i] = exp(buf[i].second)/sumBFs;
    }
    for (unsigned int i = 0; i < p; ++i)
        qs[i] = qs[i]/sumBFs;
    return Rcpp::List::create(Rcpp::_["model.vars"] = models,
                              Rcpp::_["model.probs"] = probs,
                              Rcpp::_["inc.probs"] = qs);
}
