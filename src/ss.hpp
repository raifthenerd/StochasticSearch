#ifndef SSHEADER_H
#define SSHEADER_H

#define ARMA_USE_LAPACK
#define ARMA_USE_BLAS
#define ARMA_USE_CXX11
#define ARMA_NO_DEBUG
#define ARMA_DONT_PRINT_ERRORS

#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]
// [[Rcpp::plugins(cpp11)]]
#include <map>
#include <set>
#include <vector>
#include <list>
#include <algorithm>


inline arma::vec residual (const arma::vec& y, const arma::mat& X)
{
    arma::mat Xt = X.t();
    return y-X*arma::inv(Xt*X)*Xt*y;
}

inline arma::mat modelMat (const arma::mat& X, const std::set<unsigned int> i)
{
    std::vector<unsigned int> idx (i.begin(), i.end());
    idx.push_back(0);
    return X.cols(arma::uvec(idx));
}

inline double sampleUnif (double max) {
    return R::runif(0.0, max);
}

inline bool coinToss (double p) {
    return sampleUnif(1.0) < p;
}

unsigned int sampleCdf (const std::vector<double>&);
unsigned int samplePdf (const std::vector<double>&);
double ssr (const arma::vec&, const arma::mat&);
double modelLogBF (const arma::vec&, const arma::mat&,
                   const std::set<unsigned int>&);

#endif
