# StochasticSearch

Implementation of various stochastic searches for variable selection in Bayesian
linear models.

**Note** This work is done under the undergraduate internship program at SNU
Bayesian Lab; no practical uses are expected.

## List of implemented algorithms

- Berger & Molina (2005). Posterior model probabilities via path-based pairwise
  priors.
- Scott & Carvalho (2008). Feature-inclusion stochastic search for Gaussian
  graphical models.
- Hans et al. (2007). Shotgun stochastic search for "large p" regression.
- Clyde et al. (2009). Bayesian adaptive sampling for variable selection and
  model averaging.